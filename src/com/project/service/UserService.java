package com.project.service;

import com.project.Dao.Unused_idDao;
import com.project.Dao.UserDao;
import com.project.util.DatabaseUtil;
import com.project.util.ErrorNum;
import com.project.util.FormatUtil;
import com.project.vo.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserService {

    public static long register(String name,String password,String phone,String email) {
        if(name==null || password==null || phone==null || email==null) return ErrorNum.PARAMS_IS_NULL;

        if(!FormatUtil.phoneFormat(phone)) return ErrorNum.PHONE_FORMAT_ERROR;  //手机号格式错误
        if(!FormatUtil.emailFormat(email)) return ErrorNum.EMAIL_FORMAT_ERROR;  //邮箱格式错误

        int idfCode = UserDao.HavePhoneOrEmail(phone,email);
        if(idfCode == 0) {
            String id = Unused_idDao.getId(1);
            User user = new User(id,name,password,phone,email);

            String sqlCreate = "CREATE TABLE `bill_" + id + "` (\n" +
                    "  `id` char(14) NOT NULL,\n" +
                    "  `income_or_expense` varchar(7) NOT NULL,\n" +
                    "  `number` int(11) NOT NULL,\n" +
                    "  `time` datetime NOT NULL,\n" +
                    "  `content` varchar(20) DEFAULT NULL,\n" +
                    "  PRIMARY KEY (`id`)\n" +
                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
            PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sqlCreate);
            if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
            try {
                UserDao.insertUser(user);
                pstat.executeUpdate(sqlCreate);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return ErrorNum.CATCH_EXCEPTION;
            }finally {
                try {
                    pstat.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            return Long.parseLong(id);
        }
        return idfCode;
    }

}
