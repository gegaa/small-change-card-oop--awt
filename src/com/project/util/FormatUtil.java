package com.project.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormatUtil {
    private static final Pattern email_format = Pattern.compile("^([a-zA-Z\\d][\\w-]{2,})@(\\w{2,})\\.([a-z]{2,})(\\.[a-z]{2,})?$");
    private static final Pattern phone_format = Pattern.compile("^[1][3,4,5,6,7,8,9][0-9]{9}$");
    private static final Pattern idcard_pattern = Pattern.compile("(^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|" +
            "(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|" +
            "(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}$)");
    private static final Pattern mooneyNum_format = Pattern.compile("(^[1-9](\\d+)?(\\.\\d{1,2})?$)|(^0$)|(^\\d\\.\\d{1,2}$)");  //^[1-9][0-9]*[.[0-9]{2}]$

    public static boolean emailFormat(String email) {
        Matcher matcher = email_format.matcher(email);
        if(!matcher.find()) return false;
        return true;
    }

    public static boolean phoneFormat(String phone) {
        Matcher matcher = phone_format.matcher(phone);
        if(!matcher.find()) return false;
        return true;
    }

    public static boolean idcardFormat(String idcard) {
        Matcher matcher = idcard_pattern.matcher(idcard);
        if(!matcher.find()) return false;
        return true;
    }

    public static boolean moneyNumFormat(String moneyNum) {
        Matcher matcher = mooneyNum_format.matcher(moneyNum);
        if(!matcher.find()) return false;
        return true;
    }
}
