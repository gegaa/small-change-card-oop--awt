package com.project.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

public class DatabaseUtil {
    private static Properties properties = new Properties();
    private static Connection conn = null;

    static {
        try {
            properties.load(new FileInputStream(System.getProperty("user.dir") + "\\resource\\jdbc.properties"));
            Class.forName(properties.getProperty("jdbc.driver"));
            conn = DriverManager.getConnection(properties.getProperty("jdbc.url"),
                    properties.getProperty("jdbc.name"),properties.getProperty("jdbc.password"));
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return conn;
    }

    public static void reload() {
        try {
            properties.load(new FileInputStream(System.getProperty("user.dir") + "\\resources\\jdbc.properties"));
            Class.forName(properties.getProperty("jdbc.driver"));
            conn = DriverManager.getConnection(properties.getProperty("jdbc.url"),
                    properties.getProperty("jdbc.name"),properties.getProperty("jdbc.password"));
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static PreparedStatement getPreparedStatement(Connection conn, String sql, ArrayList<Object> params) {
        try {
            PreparedStatement pstat = conn.prepareStatement(sql);
            for(int i = 0 ; i < params.size() ; i++) {
                pstat.setObject(i+1,params.get(i));
            }
            return pstat;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public static PreparedStatement getPreparedStatement(String sql) {
        try {
            PreparedStatement pstat = conn.prepareStatement(sql);
            return pstat;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public static PreparedStatement getPreparedStatementNoTry(Connection conn, String sql, ArrayList<Object> params)
            throws SQLException {
        PreparedStatement pstat = conn.prepareStatement(sql);
        for(int i = 0 ; i < params.size() ; i++) {
            pstat.setObject(i+1,params.get(i));
        }
        return pstat;
    }

    public static PreparedStatement getPreparedStatementNoTry(String sql) throws SQLException {
        return conn.prepareStatement(sql);
    }


}
