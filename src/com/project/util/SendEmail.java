package com.project.util;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.Random;

public class SendEmail {
    private static Properties p = new Properties();
    private static Session s = null;
    private static Transport t = null;
    private static MimeMessage mimeMessage = null;
    private static PreparedStatement pstat = null;

    static {
        try {
            pstat = DatabaseUtil.getPreparedStatement(
                    "insert into identifying_table(id,identify_code,startTime,endTime,type,user_id) values(?,?,?,?,?,?)"
            );

            p.load(new FileInputStream(System.getProperty("user.dir") + "\\resource\\mail.properties"));
            s = Session.getDefaultInstance(p, new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(p.getProperty("mail.sender"),p.getProperty("mail.code"));
                }
            });

            s.setDebug(false);

            //获取连接对象
            t = s.getTransport();

            //连接服务器
            t.connect(p.getProperty("mail.host"),p.getProperty("mail.sender"),p.getProperty("mail.code"));

            //创建邮件对象
            mimeMessage = new MimeMessage(s);

            //邮件发送人
            mimeMessage.setFrom(new InternetAddress(p.getProperty("mail.sender")));

            //邮件标题
            mimeMessage.setSubject("小小零钱卡");
        } catch (IOException | NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public static String sendEmail(String recipientEmail,String type,String account_id) {

        //邮件接收人
        try {
            mimeMessage.setRecipient(Message.RecipientType.TO,new InternetAddress(recipientEmail));

            //邮件内容
            Random random = new Random();
            String result="";
            for (int i=0;i<6;i++)
            {
                result+=random.nextInt(10);
            }
            Timestamp current = new Timestamp(System.currentTimeMillis()); //TODO:存在误差，但可以忽略
            LocalDateTime added= LocalDateTime.now().minus(-5, ChronoUnit.MINUTES);
            Timestamp end = new Timestamp(added.toInstant(ZoneOffset.of("+8")).toEpochMilli());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            pstat.setString(1,sdf.format(current));
            pstat.setString(2,result);
            pstat.setTimestamp(3,current);
            pstat.setTimestamp(4,end);
            pstat.setString(5,type);
            pstat.setString(6,account_id);
            if(pstat.executeUpdate() != 1) {
//                pstat.close();
                return "error";
            }
            mimeMessage.setContent("验证码：" + result,"text/html;charset=UTF-8");

            //发送邮件
            t.sendMessage(mimeMessage,mimeMessage.getAllRecipients());

            return sdf.format(current);
        } catch (MessagingException | SQLException e) {
            e.printStackTrace();
            return null;
        }finally {
//            try {
//                t.close();
//                pstat.close();
//            } catch (MessagingException | SQLException e) {
//                e.printStackTrace();
//            }
        }
    }

    public static boolean identifyEmail(String identifyId,String identifyCode) {
        if (identifyCode == null) return false;
        String sql = "select identify_code,endTime from identifying_table where id=?";
        PreparedStatement p = DatabaseUtil.getPreparedStatement(sql);
        if (p == null) return false;
        ResultSet rs = null;
        try {
            p.setString(1, identifyId);
            rs = p.executeQuery();
            if (!rs.next() || rs.getTimestamp("endTime").getTime() < System.currentTimeMillis())
                return false;
            if (identifyCode.equals(rs.getString("identify_code")))
                return true;
            else
                return false;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        } finally {

            try {
                if (rs != null)
                    rs.close();
                p.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
