package com.project.util;

public class ErrorNum {
    public static final byte PARAMS_IS_NULL = -1;
    public static final byte CATCH_EXCEPTION = -2;
    public static final byte RESULT_IS_NULL = -3;
    public static final byte STATEMENT_IS_NULL = -4; //获取的PreparedStatement对象为null
    public static final byte PHONE_HAVE = -5;
    public static final byte EMAIL_HAVE = -6;
    public static final byte PHONE_FORMAT_ERROR = -7;
    public static final byte EMAIL_FORMAT_ERROR = -8;
    public static final byte ERROE = -9;
    public static final byte ID_FORMAT_ERROR = -10;
    public static final byte INCOME_EXPENSE_FORMAT_ERROR = -11;
    public static final byte MONTH_NUM_FORMAT_ERROR = -12;

    public static String getErrorInformation(byte num) {
        switch (num) {
            case -1:
                return "传入的参数为null";
            case -2:
                return "捕获到异常";
            case -3:
                return "返回结果为null";
            case -4:
                return "PreparedStatement为null";
            case -5:
                return "手机号已存在";
            case -6:
                return "邮箱已存在";
            case -10:
                return "用户ID格式错误";
            case -11:
                return "请选择收入或支出类型";
            case -12:
                return "月数不对";
            default:
                return "未知问题";
        }
    }
}
