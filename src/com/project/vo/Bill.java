package com.project.vo;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Bill {
    private String id;
    private String type;
    private BigDecimal moneyNum;
    private Timestamp createTime;
    private String content;

    public Bill(String id, String type, BigDecimal moneyNum, Timestamp createTime, String content) {
        this.id = id;
        this.type = type;
        if("收入".equals(type))
            this.moneyNum = moneyNum;
        if("支出".equals(type))
            this.moneyNum = moneyNum.negate();
        this.createTime = createTime;
        this.content = content;
    }

    public Bill(String type, BigDecimal moneyNum, String content) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddHHmmss");
        Timestamp current = new Timestamp(System.currentTimeMillis());
        this.id = s.format(current);
        this.type = type;
        if("收入".equals(type)) {
            this.moneyNum = moneyNum;
        }
        if("支出".equals(type)) {
            this.moneyNum = moneyNum.negate();
        }

        this.createTime = current;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getMoneyNum() {
        return moneyNum;
    }

    public void setMoneyNum(BigDecimal moneyNum) {
        this.moneyNum = moneyNum;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
