package com.project.vo;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;

public class User {
    private String id;
    private String username;
    private String name;
    private String password;
    private String idcard;
    private String phone;
    private String email;
    private BigDecimal balance;
    private Timestamp createTime;
    private Timestamp vipEnd;

    public User(String id, String username, String name, String password, String idcard, String phone,
                String email, BigDecimal balance, Timestamp vipEnd,Timestamp createTime ) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.password = password;
        this.idcard = idcard;
        this.phone = phone;
        this.email = email;
        this.balance = balance;
        this.createTime = createTime;
        this.vipEnd = vipEnd;
    }

    /**
     * 新建用户专门使用的构造方法
     * @param id
     * @param username
     * @param password
     * @param phone
     * @param email
     */
    public User(String id,String username,String password,String phone,String email) {
        this(id,username,"",password,"",phone,email,new BigDecimal(0),
                new Timestamp(System.currentTimeMillis()),new Timestamp(System.currentTimeMillis()));
    }

    /**
     * 查询用户专门使用的构造方法(big)
     * @param id
     * @param username
     * @param name
     * @param idcard
     * @param phone
     * @param email
     * @param balance
     * @param vipEnd
     */
    public User(String id,String username,String name,String idcard,String phone,
                String email,BigDecimal balance,Timestamp vipEnd) {
        this(id,username,name,"******",idcard,   //idcard.substring(0,4)+"**********" +idcard.substring(13,17)
                phone,email,balance,vipEnd,null);
    }

    /**
     * 查询用户专门使用的构造方法(small)
     * @param id
     * @param username
     * @param name
     * @param idcard
     * @param phone
     * @param email
     */
    public User(String id,String username,String name,String idcard,String phone,
                String email) {
        this(id,username,name,"******",idcard,
                phone,email,null,null,null);
    }

    public User(String id, BigDecimal balance, Timestamp vipEnd) {
        this.id = id;
        this.username = this.name = this.password = this.idcard = this.phone = this.email  = null;
        this.createTime = null;
        this.balance = balance;
        this.vipEnd = vipEnd;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setVipEnd(Timestamp vipEnd) {
        this.vipEnd = vipEnd;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getIdcard() {
        return idcard;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public Timestamp getVipEnd() {
        return vipEnd;
    }
}
