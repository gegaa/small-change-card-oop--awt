package com.project.Dao;

import com.project.util.DatabaseUtil;
import com.project.util.ErrorNum;
import com.project.vo.User;

import java.math.BigDecimal;
import java.sql.*;

/**
 *  @Author sterarpen
 *  @Date 2023-4-1
 *  @Description 数据库用户更新相关类
 *  @Version 1.0.0
 */
public class UserDao {
    /**
     * 向数据库新建用户
     * @param user 用户对象
     * @return 1-插入成功，0-参数user为null
     */
    public static int insertUser(User user) {
        if(user == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "insert into user(id,username,password,phone,email,createTime,vipEnd) values(?,?,?,?,?,?,?)";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setString(1,user.getId());
            pstat.setString(2,user.getUsername());
            pstat.setString(3,user.getPassword());
            pstat.setString(4,user.getPhone());
            pstat.setString(5,user.getEmail());
            pstat.setTimestamp(6,user.getCreateTime());
            pstat.setTimestamp(7,user.getVipEnd());
            return pstat.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        } finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    /**
     * 根据ID删除用户
     * @param userId
     * @return
     */
    public static int deleteUser(String userId) {
        if(userId == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "delete from user where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setString(1,userId);
            return pstat.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        } finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    /**
     * 查看是否存在对应ID的用户
     * @param userId
     * @return
     */
    public static int HaveUser(String userId) {
        if(userId == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "select count(*) as cout from user where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.CATCH_EXCEPTION;
        try {
            pstat.setString(1,userId);
            ResultSet rs = pstat.executeQuery();
            if(!rs.next()) return 0;
            return rs.getInt("cout");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        }finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }

    /**
     * 用户登录数据库查询
     * @param userId
     * @param password
     * @return
     */
    public static int LoginUser(String userId,String password) {
        if(userId == null || password == null) {
            return ErrorNum.PARAMS_IS_NULL;
        }
        String sql = "select count(*) as cout from user where id=? and password=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setString(1,userId);
            pstat.setString(2,password);
            ResultSet rs = pstat.executeQuery();
            if(!rs.next()) return 0;
            return rs.getInt("cout");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        } finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static User queryUser(String userId) {
        if(userId == null) {
            return null;
        }
        String sql = "select username,name,idcard,phone,email from user where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return null;
        try {
            pstat.setString(1,userId);
            ResultSet rs = pstat.executeQuery();
            if(!rs.next()) return null;
            String username = rs.getString("username");
            String name = rs.getString("name");
            String idcard = rs.getString("idcard");
            String phone = rs.getString("phone");
            String email = rs.getString("email");
            User user = new User(userId,username,name,idcard,phone,email);
            rs.close();
            return user;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        } finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static User queryUserComplex(String userId) {
        if(userId == null) {
            return null;
        }
        //balance、vipEnd可以去除
        String sql = "select username,name,idcard,phone,email,balance,vipEnd from user where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return null;
        try {
            pstat.setString(1,userId);
            ResultSet rs = pstat.executeQuery();
            if(!rs.next()) return null;
            String username = rs.getString("username");
            String name = rs.getString("name");
            String idcard = rs.getString("idcard");
            String phone = rs.getString("phone");
            String email = rs.getString("email");
            BigDecimal balance = rs.getBigDecimal("balance");
            Timestamp vipEnd = rs.getTimestamp("vipEnd");
            User user = new User(userId,username,name==null?"":name,idcard==null?"":
                    (idcard.substring(0,4)+"**********" + idcard.substring(14,17)),phone,email,balance,vipEnd);
            rs.close();
            return user;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        } finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }

    public static int userInformationIsReplace(User user) {
        if(user == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "select * from user where username=? and name=? and idcard=? and phone=? and email=? and id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        ResultSet rs = null;
        try {
            pstat.setString(1,user.getUsername());
            pstat.setString(2,user.getName());
            pstat.setString(3,user.getIdcard());
            pstat.setString(4,user.getPhone());
            pstat.setString(5,user.getEmail());
            pstat.setString(6,user.getIdcard());
            rs =  pstat.executeQuery();
            if(rs == null) return ErrorNum.RESULT_IS_NULL;
            if(rs.next()) return 0;  //未修改返回0
            else return 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        }finally {
            try {
                rs.close();
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }

    /**
     * 手机号和邮箱是否已注册数据库查询
     * @param phone
     * @param email
     * @return 0-手机号和邮箱未被注册
     */
    public static int HavePhoneOrEmail(String phone,String email) {
        if(phone == null || email == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "select count(*) from user where phone=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        ResultSet rs = null;
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setString(1,phone);
            rs = pstat.executeQuery();
            if(!rs.next()) return ErrorNum.ERROE;
            if(rs.getInt(1) >= 1) return ErrorNum.PHONE_HAVE;
            rs.close();
            pstat.close();
            sql = "select count(*) from user where email=?";
            pstat = DatabaseUtil.getPreparedStatement(sql);
            pstat.setString(1,email);
            rs = pstat.executeQuery();
            if(!rs.next()) return ErrorNum.ERROE;
            if(rs.getInt(1) >= 1) return ErrorNum.EMAIL_HAVE;
            return 0;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        } finally {
            try {
                rs.close();
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

    }

    public static int updateUserInformation(User user) {
        if(user == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "update user set username=?,name=?,idcard=?,phone=?,email=? where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setString(1,user.getUsername());
            pstat.setString(2,user.getName());
            pstat.setString(3,user.getIdcard());
            pstat.setString(4,user.getPhone());
            pstat.setString(5,user.getEmail());
            pstat.setString(6,user.getId());
            if(pstat.executeUpdate() != 1) return ErrorNum.RESULT_IS_NULL;
            return 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        }finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public static int updateUserBalance(User user) {
        if(user == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "update user set balance=? where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setBigDecimal(1,user.getBalance());
            pstat.setString(2,user.getId());
            if(pstat.executeUpdate() != 1) return ErrorNum.RESULT_IS_NULL;
            return 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        }finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    /**
     * 用户VIP充值，数据库操作
     * @param user 获取充值VIP后VIP日期和剩余余额的用户类
     * @return 操作结果，是否成功，成功返回1，失败返回对应失败异常码
     */
    public static int updateUserVip(User user) {
        if(user == null) return ErrorNum.PARAMS_IS_NULL;
        String sql = "update user set vipEnd=?,balance=? where id=?";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setTimestamp(1,user.getVipEnd());
            pstat.setBigDecimal(2,user.getBalance());
            pstat.setString(3,user.getId());
            if(pstat.executeUpdate() != 1) return ErrorNum.RESULT_IS_NULL;
            return 1;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        }finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }


}
