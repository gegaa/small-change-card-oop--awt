package com.project.Dao;

import com.project.util.DatabaseUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Unused_idDao {
    public static final byte NORMAL = 1;

    public static String getId(int type) {
        String sql = "select id from unused_id where type=? limit 0,1";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        ResultSet rs = null;
        if(pstat == null) return null;
        try {
            pstat.setInt(1,type);
            rs = pstat.executeQuery();
            if(!rs.next()) return null;
            String id = rs.getString("id");
            rs.close();
            pstat.close();

            if(id.length() != 11) return null;
            sql = "delete from unused_id where id=?";
            pstat = DatabaseUtil.getPreparedStatement(sql);
            if(pstat == null) return null;
            pstat.setString(1,id);
            if(pstat.executeUpdate() != 1) return null;
            return id;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }finally {
            try {
                rs.close();
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }


}
