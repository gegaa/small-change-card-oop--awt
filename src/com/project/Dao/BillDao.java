package com.project.Dao;

import com.project.util.DatabaseUtil;
import com.project.util.ErrorNum;
import com.project.vo.Bill;
import com.project.vo.User;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BillDao {
    public static int inputBill(Bill bill,String userId) {
        if(bill == null) return ErrorNum.PARAMS_IS_NULL;
        for(int i = 0 ; i < userId.length() ; i++) {
            if(!Character.isDigit(userId.charAt(i)))
                return ErrorNum.ID_FORMAT_ERROR;
        }
        String sql = "insert into Bill_" + userId + "(id, income_or_expense,number,time,content) values(?,?,?,?,?)";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
        try {
            pstat.setString(1,bill.getId());
            pstat.setString(2,bill.getType());
            pstat.setBigDecimal(3,bill.getMoneyNum());
            pstat.setTimestamp(4,bill.getCreateTime());
            pstat.setString(5,bill.getContent());
            if(pstat.executeUpdate() != 1)
                return ErrorNum.ERROE;
//            String updateUser = "update user set balance=? where id=?";
//            pstat.close();
//            pstat = DatabaseUtil.getConnection().prepareStatement(updateUser);
//            if(pstat == null) return ErrorNum.STATEMENT_IS_NULL;
//            pstat.setBigDecimal(1,bill.getMoneyNum());
//            pstat.setString(2,userId);
//            if(pstat.executeUpdate() != 1)
//                return ErrorNum.ERROE;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return ErrorNum.CATCH_EXCEPTION;
        }finally {
            try {
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return 1;
    }

    public static Bill[] queryBill(int pageNum, User user) {
        Bill[] bills = new Bill[16];
        if(pageNum < 0) return null;
        String sql = "select income_or_expense,number,time,content from bill_" + user.getId() + " order by time desc limit " + pageNum + ",16";
        PreparedStatement pstat = DatabaseUtil.getPreparedStatement(sql);
        if(pstat == null) return null;
        ResultSet rs = null;
        try {
            rs = pstat.executeQuery();
            if(rs == null) return null;
            int i = 0;
            if(!rs.next()) return null;

             do {
                bills[i] = new Bill("11111111111",rs.getString("income_or_expense"),rs.getBigDecimal("number"),
                        rs.getTimestamp("time"),rs.getString("content"));
                i++;
            }while(rs.next());
            return bills;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }finally {
            try {
                rs.close();
                pstat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }
}
