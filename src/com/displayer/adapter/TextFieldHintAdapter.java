package com.displayer.adapter;

import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class TextFieldHintAdapter implements FocusListener {
    private String defaultContent;
    private TextField textField;

    public TextFieldHintAdapter(String defaultContent,TextField textField) {
        this.defaultContent = defaultContent;
        this.textField = textField;
        this.textField.setText(this.defaultContent);
        this.textField.setForeground(Color.GRAY);
    }

    @Override
    public void focusGained(FocusEvent e) {
        String temp = this.textField.getText();
        if(defaultContent.equals(temp)) {
            this.textField.setText("");
            this.textField.setForeground(Color.black);
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        String temp = this.textField.getText();
        if("".equals(temp)) {
            this.textField.setText(this.defaultContent);
            this.textField.setForeground(Color.gray);
        }
    }
}
