package com.displayer.window;

import com.mysql.cj.util.StringUtils;
import com.project.Dao.BillDao;
import com.project.Dao.UserDao;
import com.project.util.ErrorNum;
import com.project.util.FormatUtil;
import com.project.vo.Bill;
import com.project.vo.User;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class MainWindow extends Frame implements WindowListener{

    //父窗口对象，即登录窗口对象
    private LoginWindow loginWindow;
    private Pop quitBox = null;

    private Panel[] panels = new Panel[6];
    private List list = new List();

    private Font font = new Font("仿宋",Font.PLAIN,30);
    private Label[] label_UserInformation = new Label[8];
    private TextField[] textFields = new TextField[8];
    private JTable jTable = new JTable();
    Label[] labels_input_bill = new Label[3];

    private User user;

    private Pop promptBox = null;
    private VipRechargeBox vipRechargeBox = null;
    private IdentifyBoxForMainWindow identifyBox = null;

    private Label[] billTable_head = new Label[4];
    private List[] billTable = new List[4];
    private Button[] up_down_page = new Button[2];

    private int pageNum = 0;
    private Bill[] bills;


    public MainWindow(LoginWindow loginWindow,User user) {
        this.loginWindow = loginWindow;
        this.user = user;
        this.bills = BillDao.queryBill(this.pageNum,user);

        try {
            setIconImage(ImageIO.read(new File(System.getProperty("user.dir") + "\\resource\\picture\\icon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setTitle("主页面");
        this.setSize(1650,800);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());

        //帐单列表页面相关组件初始化
        for(int i = 0 ; i < billTable.length ; i++) {
            billTable_head[i] = new Label();
            billTable_head[i].setFont(font);
            billTable[i] = new List();
            billTable[i].setFont(font);
        }
        for(int i = 0 ; i < up_down_page.length ; i++) {
            up_down_page[i] = new Button();
            up_down_page[i].setFont(font);
        }
        up_down_page[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(pageNum > 0) {
                    pageNum --;
                    bills = BillDao.queryBill(pageNum*16,user);
                    selectList();
                    panels[1].setVisible(true);
                }
            }
        });
        up_down_page[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int pageNumTemp = pageNum + 1;
                Bill[] billsTemp = null;
                if((billsTemp = BillDao.queryBill(pageNumTemp*16,user)) != null && billsTemp.length > 0) {
                    pageNum = pageNumTemp;
                    bills = billsTemp;
                    selectList();
                    panels[1].setVisible(true);
                }
            }
        });

        Panel listUpper = new Panel();
        listUpper.setBounds(0,0,400,200);
        listUpper.setBackground(new Color(149,171,240));
        listUpper.setLayout(null);

        Label imageIcon = new Label() {
            public void paint(Graphics g) {
                BufferedImage image;
                try {
                    image = ImageIO.read(new File(System.getProperty("user.dir") + "\\resource\\picture\\user.png"));
                    g.drawImage(image, 0, 0, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        imageIcon.setBounds(180,10,40,40);
        listUpper.add(imageIcon);

        Label label_username = new Label(user.getUsername());
        label_username.setBounds(50,60,300,40);
        label_username.setFont(new Font("仿宋",Font.PLAIN,40));
        label_username.setAlignment(Label.CENTER);
        label_username.setBackground(new Color(149,171,240));
        label_username.setForeground(Color.white);
        listUpper.add(label_username);


        Label label_vip = new Label("VIP");
        label_vip.setBounds(180,100,40,20);
        label_vip.setFont(new Font("仿宋",Font.BOLD,20));
        label_vip.setAlignment(Label.CENTER);
        label_vip.setBackground(new Color(200,171,240));
        if(isVip())
            label_vip.setForeground(Color.yellow);
        else
            label_vip.setForeground(Color.gray);

        listUpper.add(label_vip);


        list.setBounds(0,200,400,600);
        list.setFont(new Font("仿宋", Font.PLAIN, 40));
        list.add("基本信息");
        list.add("查看账单");
        list.add("加入账单");
        list.add("VIP功能");
        list.add("退出登录");
        list.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                newUser();
                selectList();
                panels[1].setVisible(true);

            }
        });

        for (int i = 0 ; i < panels.length ; i++)
            panels[i] = new Panel();

        panels[0].setPreferredSize(new Dimension(400,800));
        panels[0].setLocation(0,0);
        panels[0].setLayout(null);
        panels[0].add(listUpper);
        panels[0].add(list);

        panels[1].setPreferredSize(new Dimension(1250,800));
        panels[1].setBackground(Color.BLUE);
        panels[1].setLayout(null);

        this.add(panels[0],BorderLayout.WEST);
        this.add(panels[1],BorderLayout.CENTER);

        this.setVisible(true);

        this.addWindowListener(this);

        for(int i = 0 ; i < label_UserInformation.length ; i++) {
            label_UserInformation[i] = new Label();
            label_UserInformation[i].setFont(font);
            label_UserInformation[i].setSize(100,30);
            label_UserInformation[i].setAlignment(Label.CENTER);
        }

        for(int i = 0 ; i < labels_input_bill.length ;i++) {
            labels_input_bill[i] = new Label();
            labels_input_bill[i].setFont(font);
        }
    }

    public static void main(String[] args) {
        new MainWindow(null,new User("11111111111","kaili","name",
                "468515754555325685",
                "16587456854",
                "2584765843@qq.com",
                new BigDecimal(100),
                Timestamp.valueOf("2023-12-12 11:12:12")
        ));
    }

    public boolean isVip() {
        if(user.getVipEnd() == null) return false;
        if(new Timestamp(System.currentTimeMillis()).before(user.getVipEnd()))
            return true;
        else
            return false;
    }


    public void selectList() {
//        user = UserDao.queryUserComplex(user.getId());
        panels[1].removeAll();
        switch (list.getSelectedIndex()) {
            case 0:
                label_UserInformation[0].setText("账号: ");
                label_UserInformation[1].setText("用户名: ");
                label_UserInformation[2].setText("姓名: ");
                label_UserInformation[3].setText("身份证号: ");
                label_UserInformation[4].setText("余额: ");
                label_UserInformation[5].setText("VIP: ");
                label_UserInformation[6].setText("手机号: ");
                label_UserInformation[7].setText("邮箱: ");

                label_UserInformation[0].setLocation(50,55);
                panels[1].add(label_UserInformation[0]);
                label_UserInformation[1].setLocation(640,55);
                panels[1].add(label_UserInformation[1]);
                label_UserInformation[2].setLocation(50,200);
                panels[1].add(label_UserInformation[2]);
                label_UserInformation[3].setLocation(610,200);
                label_UserInformation[3].setSize(130,30);
                panels[1].add(label_UserInformation[3]);
                label_UserInformation[4].setLocation(50,345);
                panels[1].add(label_UserInformation[4]);
                label_UserInformation[5].setLocation(640,345);
                panels[1].add(label_UserInformation[5]);
                label_UserInformation[6].setLocation(40,490);
                label_UserInformation[6].setSize(110,30);
                panels[1].add(label_UserInformation[6]);
                label_UserInformation[7].setLocation(640,490);
                panels[1].add(label_UserInformation[7]);

                for(int i = 0 ; i < textFields.length ; i++) {
                    textFields[i] = new TextField();
                    textFields[i].setFont(font);
                    textFields[i].setSize(400,35);
                }
                textFields[0].setText(user.getId());
                textFields[0].setEnabled(false);
                textFields[0].setLocation(150,50);
                panels[1].add(textFields[0]);
                textFields[1].setText(user.getUsername());
                textFields[1].setLocation(750,50);
                panels[1].add(textFields[1]);
                textFields[2].setText(user.getName()==null?"":user.getName());
                textFields[2].setLocation(150,195);
                panels[1].add(textFields[2]);
                textFields[3].setText(user.getIdcard()==null?"":user.getIdcard());
                textFields[3].setLocation(750,195);
                panels[1].add(textFields[3]);
                textFields[4].setText(user.getBalance().toString());
                textFields[4].setLocation(150,340);
                textFields[4].setEnabled(false);
                panels[1].add(textFields[4]);
                textFields[5].setText(user.getVipEnd()==null?"":new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(user.getVipEnd()));
                textFields[5].setEnabled(false);
                textFields[5].setLocation(750,340);
                panels[1].add(textFields[5]);
                textFields[6].setText(user.getPhone());
                textFields[6].setLocation(150,485);
                panels[1].add(textFields[6]);
                textFields[7].setText(user.getEmail());
                textFields[7].setLocation(750,485);
                panels[1].add(textFields[7]);

                Button button = new Button("修改信息");
                button.setBounds(300,630,600,50);
                button.setFont(new Font("仿宋",Font.PLAIN,40));
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String username = textFields[1].getText();
                        String name = textFields[2].getText();
                        String idcard = textFields[3].getText();
                        String phone = textFields[6].getText();
                        String email = textFields[7].getText();
                        if(!user.getPhone().equals(phone)) { //TODO:手机号修改验证码验证
//                            newIdentifyBox();
                        }
                        if(!user.getEmail().equals(email)) {
                            newIdentifyBox(2,email);
                        }
                        if(!(StringUtils.nullSafeEqual(username,user.getUsername()) && StringUtils.nullSafeEqual(name,user.getName()) &&
                            StringUtils.nullSafeEqual(idcard,user.getIdcard()))) {
                            if(!FormatUtil.idcardFormat(idcard)) {
                                newPromptBox("身份证格式错误");
                                return;
                            }
                            int resultCode = UserDao.updateUserInformation(new User(user.getId(),username,name,idcard,phone,email));
                            if(resultCode == 1) {
                                newPromptBox("修改成功");
                            }else {
                                newPromptBox(ErrorNum.getErrorInformation((byte)resultCode));
                            }
                        }
                    }
                });
                panels[1].add(button);
                break;
            case 1:
                up_down_page[0].setLabel("上一页");
                up_down_page[0].setBounds(50,670,200,60);
                panels[1].add(up_down_page[0]);
                up_down_page[1].setLabel("下一页");
                up_down_page[1].setBounds(1000,670,200,60);
                panels[1].add(up_down_page[1]);

                billTable_head[0].setText("类型");
                billTable_head[0].setBounds(50,10,100,50);
                billTable_head[0].setAlignment(Label.CENTER);
                panels[1].add(billTable_head[0]);
                billTable_head[1].setText("金额");
                billTable_head[1].setBounds(150,10,200,50);
                billTable_head[1].setAlignment(Label.CENTER);
                panels[1].add(billTable_head[1]);
                billTable_head[2].setText("日期时间");
                billTable_head[2].setBounds(350,10,400,50);
                billTable_head[2].setAlignment(Label.CENTER);
                panels[1].add(billTable_head[2]);
                billTable_head[3].setText("内容");
                billTable_head[3].setBounds(750,10,450,50);
                billTable_head[3].setAlignment(Label.CENTER);
                panels[1].add(billTable_head[3]);

                for(int i = 0 ; i < 4 ; i++)
                    billTable[i].removeAll();
                for(int i = 0 ; i < bills.length ; i++) {
                    if(bills[i] == null) break;
                    billTable[0].add(bills[i].getType());
                    billTable[1].add(bills[i].getMoneyNum().toString());  //TODO:
                    billTable[2].add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(bills[i].getCreateTime()));
                    billTable[3].add(bills[i].getContent());
                }

                billTable[0].setBounds(50,60,100,600);
                billTable[0].setBackground(Color.RED);
                panels[1].add(billTable[0]);
                billTable[1].setBounds(150,60,200,600);
                billTable[1].setBackground(Color.CYAN);
                panels[1].add(billTable[1]);
                billTable[2].setBounds(350,60,400,600);
                billTable[2].setBackground(Color.blue);
                panels[1].add(billTable[2]);
                billTable[3].setBounds(750,60,450,600);
                billTable[3].setBackground(Color.green);
                panels[1].add(billTable[3]);
//                for(int i = 0 ; i < 16 ; i++)
//                     billTable[0].add("收入");
                break;
            case 2:
                labels_input_bill[0].setText("收入或支出: ");
                labels_input_bill[0].setBounds(320,100,170,35);
                panels[1].add(labels_input_bill[0]);
                labels_input_bill[1].setText("金            额: ");
                labels_input_bill[1].setBounds(320,200,170,30);
                panels[1].add(labels_input_bill[1]);
                labels_input_bill[2].setText("内            容: ");
                labels_input_bill[2].setBounds(320,300,170,30);
                panels[1].add(labels_input_bill[2]);
                Choice type_choice = new Choice();
                type_choice.setFont(font);
                type_choice.add("收入");
                type_choice.add("支出");
                type_choice.setBounds(495,98,400,32);
                panels[1].add(type_choice);
                TextField num_textfield = new TextField();
                num_textfield.setFont(font);
                num_textfield.setBounds(495,200,400,35);
                panels[1].add(num_textfield);
                TextField content_textfield = new TextField();
                content_textfield.setFont(font);
                content_textfield.setBounds(495,300,400,35);
                panels[1].add(content_textfield);

                Button replaceOk = new Button("确定");
                replaceOk.setFont(font);
                replaceOk.setBounds(405,450,400,42);
                panels[1].add(replaceOk);
                replaceOk.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String moneyNum = num_textfield.getText();
                        if(moneyNum==null || !FormatUtil.moneyNumFormat(moneyNum)){
                            newPromptBox("金额格式错误");
                        }else {
                            if("支出".equals(type_choice.getSelectedItem())) {
                                if(user.getVipEnd().getTime() - System.currentTimeMillis() <= 0) {  //非vip
                                    System.out.println("非VIP");
                                    if(user.getBalance().doubleValue() -  Double.valueOf(content_textfield.getText()) < 0) {  //如果用户余额小于支出金额，支出记录加入账单失败
                                        newPromptBox("您的余额不足！可通过升级为VIP进行透支消费");
                                        return;
                                    }
                                }
                            }
                             Bill bill  = new Bill(type_choice.getSelectedItem(),new BigDecimal(moneyNum),
                                        content_textfield.getText()==null?"":content_textfield.getText());
                            int resultCode = BillDao.inputBill(bill,user.getId());
                            if(resultCode == 1){
                                newPromptBox("账单加入成功");
                                user.setBalance(user.getBalance().add(bill.getMoneyNum()));
                                UserDao.updateUserBalance(new User(user.getId(),user.getBalance(),null));
//                                UserDao.uPDATE
//                                setVisible(true);
                            }
                            else
                                newPromptBox(ErrorNum.getErrorInformation((byte)resultCode));
                        }
                    }
                });

                break;
            case 3:
                TextArea vip_introduce = new TextArea();

                Label vip = new Label("VIP剩余天数: ");
                vip.setFont(font);
                vip.setBounds(420,650,200,30);
                panels[1].add(vip);

                TextField vipDay = new TextField();
                if (user.getVipEnd() == null) {
                    vipDay.setText("0");
                }
                long interval = user.getVipEnd().getTime() - (new Timestamp(System.currentTimeMillis()).getTime());
                double vipTime = (float)interval/24/60/60/1000;
                if(vipTime > 1) {
                    vipDay.setText(String.valueOf((int)vipTime));
                } else if(vipTime > 0) {
                    vipDay.setText("1");
                } else {
                    vipDay.setText("0");
                }
                vipDay.setEnabled(false);
                vipDay.setFont(font);
                vipDay.setBounds(620,650,250,35);
                panels[1].add(vipDay);

                Button vipAdd = new Button("VIP充值");
                vipAdd.setFont(font);
                vipAdd.setBounds(400,550,500,60);
                vipAdd.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        newVipRechargeBox();
                        vipRechargeBox.getVipRechargeNum();
                    }
                });
                panels[1].add(vipAdd);
                break;
            case 4:
                this.dispose();
                new LoginWindow();
                break;
        }
    }

    private void newIdentifyBox(int type,String phone_or_email) {
        if(identifyBox != null) {
            identifyBox.validation(type,phone_or_email);
        }else {
            identifyBox = new IdentifyBoxForMainWindow(this,type,phone_or_email);
        }
    }

    private void newPromptBox(String content) {
        if(promptBox != null) {
            promptBox.reloadPop(content);
        }else {
            promptBox = new Pop(this,content);
        }
    }

    private void newVipRechargeBox() {
        if(vipRechargeBox != null) {
            vipRechargeBox.setVisible(true);
        } else {
            vipRechargeBox = new VipRechargeBox(this);
        }
    }

    public void newUser() {
        this.user = UserDao.queryUserComplex(user.getId());
    }

    public Timestamp getUserVipEnd() {
        return user.getVipEnd();
    }
    public String getUserId() {
        return user.getId();
    }public BigDecimal getUserBalance() {
        return user.getBalance();
    }
    public void setUser(User user) {
        this.user = user;
    }
    public User getUser() {
        return this.user;
    }



    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.dispose();
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
