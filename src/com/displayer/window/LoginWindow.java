package com.displayer.window;


import com.project.Dao.UserDao;
import com.project.vo.User;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 *  @Author sterarpen
 *  @Date 2023-4-4
 *  @Description ��¼������
 *  @version 1.0.0
 */
public class LoginWindow extends Frame implements WindowListener {
    private RegisterWindow registerWindow = null;
    private Label[] labels = new Label[2];
    private TextField[] textFields = new TextField[2];
    private Button[] buttons = new Button[2];
    private User user = null;
    private MainWindow mainWindow = null;

    private Pop promptBox = null;

    public LoginWindow() {
        try {
            setIconImage(ImageIO.read(new File(System.getProperty("user.dir") + "\\resource\\picture\\icon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int i = 0 ; i < labels.length ; i++)
            labels[i] = new Label();
        for(int i = 0 ; i < textFields.length ; i++)
            textFields[i] = new TextField();
        for(int i = 0 ; i < buttons.length ; i++)
            buttons[i] = new Button();

        this.init();
        addWindowListener(this);
    }

    public void init() {
        setTitle("��¼");
        setSize(840,600);
        setResizable(false);
        setLocationRelativeTo(null);

        setLayout(null);

        labels[0].setText("��  ��: ");
        labels[0].setBounds(150,213,120,40);
        labels[0].setFont(new Font("����", Font.PLAIN, 30));
        labels[0].setAlignment(Label.LEFT);
        labels[0].setBackground(new Color(135,206,250));


        textFields[0].setBounds(280,213,360,40);
        textFields[0].setFont(new Font("����", Font.PLAIN, 30));

        labels[1].setText("��  ��: ");
        labels[1].setFont(new Font("����", Font.PLAIN, 30));
        labels[1].setAlignment(Label.LEFT);
        labels[1].setBounds(150,300,120,40);
        labels[1].setBackground(new Color(135,206,250));

        textFields[1].setBounds(280,300,360,40);
        textFields[1].setEchoChar('*');
        textFields[1].setFont(new Font("����", Font.PLAIN, 30));

        buttons[0].setLabel("��¼");
        buttons[0].setBounds(280,420,360,50);
        buttons[0].setFont(new Font("����", Font.PLAIN, 30));
        buttons[0].addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String id = getName();
                String password = getPassword();
                if(UserDao.LoginUser(id,password) == 1) {
                    user = UserDao.queryUserComplex(id);
                    if(mainWindow == null)
                        mainWindow = new MainWindow(getThis(),user);
                    else
                        mainWindow.setVisible(true);
                    setVisible(false);
                }
                else
                    newPromptBoxPop("�˺Ż��������");
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        buttons[1].setLabel("ע���˻�");
        buttons[1].setBounds(150,420,120,50);
        buttons[1].setFont(new Font("����", Font.PLAIN, 20));
        LoginWindow lw = this;
        buttons[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeAll();
                setVisible(false);
                if(registerWindow == null)
                    registerWindow = new RegisterWindow(lw);
                else
                    registerWindow.init();
            }
        });

        for(Label label : labels)
            add(label);
        for(TextField textField : textFields)
            add(textField);
        for(Button button : buttons) {
            add(button);
        }

        setVisible(true);
    }

    public void paint(Graphics g) {
        super.paint(g);
        BufferedImage image;
        try {
            image = ImageIO.read(new File(System.getProperty("user.dir") + "\\resource\\picture\\background.png"));
            g.drawImage(image, 0, 30, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void newPromptBoxPop(String content) {
        if(promptBox != null) {
            promptBox.reloadPop(content);
        }else {
            promptBox = new Pop(this,content);
        }
    }

    public String getName() {
        return this.textFields[0].getText();
    }

    public String getPassword() {
        return this.textFields[1].getText();
    }

    public LoginWindow getThis() {
        return this;
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        setVisible(false);
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

