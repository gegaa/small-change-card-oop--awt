package com.displayer.window;

import com.project.service.UserService;
import com.project.util.ErrorNum;
import com.project.util.FormatUtil;
import com.project.util.SendEmail;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RegisterWindow extends Frame implements WindowListener {
    private LoginWindow loginWindow;
    private Pop promptBox = null;
    private Pop identifyBox = null;
    private String identifyId = null;
    private boolean emailOk = false;

    private Label[] labels = new Label[6];
    private TextField[] textFields = new TextField[4];
    private Button[] buttons = new Button[4];

    public RegisterWindow(LoginWindow loginWindow) {
        try {
            setIconImage(ImageIO.read(new File(System.getProperty("user.dir") + "\\resource\\picture\\icon.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.loginWindow = loginWindow;
        this.init();
        addWindowListener(this);
    }

    public void init() {
        setTitle("ע��");
        setSize(840,700);
        setResizable(true);
        setLocationRelativeTo(null);

        setLayout(null);

        //0-�˺�
        //1-����
        //2-�ֻ���
        //3-����
        for(int i = 0 ; i < labels.length ; i++) {
            labels[i] = new Label();
        }

        for(int i = 0 ; i < textFields.length ; i++)
            textFields[i] = new TextField();

        for(int i = 0 ; i < buttons.length ; i++)
            buttons[i] = new Button();

        labels[0].setText("�û���: ");
        labels[0].setBounds(150,100,120,40);
        labels[0].setFont(new Font("����", Font.PLAIN, 30));
        labels[0].setAlignment(Label.LEFT);
        labels[0].setBackground(new Color(135,206,250));

        textFields[0].setBounds(280,100,360,40);
        textFields[0].setFont(new Font("����", Font.PLAIN, 30));

        labels[1].setText("��    ��: ");
        labels[1].setFont(new Font("����", Font.PLAIN, 30));
        labels[1].setAlignment(Label.LEFT);
        labels[1].setBounds(150,190,120,40);
        labels[1].setBackground(new Color(135,206,250));

        textFields[1].setBounds(280,190,360,40);
//        textFields[1].setEchoChar('*');
        textFields[1].setFont(new Font("����", Font.PLAIN, 30));

        labels[2].setText("�ֻ���: ");
        labels[2].setFont(new Font("����", Font.PLAIN, 30));
        labels[2].setAlignment(Label.LEFT);
        labels[2].setBounds(150,280,120,40);
        labels[2].setBackground(new Color(135,206,250));


        textFields[2].setBounds(280,280,360,40);
        textFields[2].setFont(new Font("����", Font.PLAIN, 30));

        labels[5].setText("�ֻ�����֤");
        labels[5].setBounds(641,298,105,20);
        labels[5].setFont(new Font("����", Font.PLAIN, 20));
        labels[5].setBackground(new Color(160, 238, 225));

        labels[5].addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                newIdentifyBoxPop(1);
            }
            @Override
            public void mousePressed(MouseEvent e) { }

            @Override
            public void mouseReleased(MouseEvent e) { }

            @Override
            public void mouseEntered(MouseEvent e) {
                labels[5].setForeground(Color.RED);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                labels[5].setForeground(Color.black);
            }
        });

        labels[3].setText("����: ");
        labels[3].setFont(new Font("����", Font.PLAIN, 30));
        labels[3].setAlignment(Label.LEFT);
        labels[3].setBounds(150,370,120,40);
        labels[3].setBackground(new Color(135,206,250));

        textFields[3].setBounds(280,370,360,40);
        textFields[3].setFont(new Font("����", Font.PLAIN, 30));

        labels[4].setText("������֤");
        labels[4].setBounds(641,389,105,20);
        labels[4].setFont(new Font("����", Font.PLAIN, 20));
        labels[4].setBackground(new Color(160, 238, 225));

        labels[4].addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String email = textFields[3].getText();
                if(email == null) {
                    newPromptBoxPop("����Ϊ��");
                }
                if(!FormatUtil.emailFormat(email)) {
                    newPromptBoxPop("�����ʽ����");
                    return;
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                identifyId = SendEmail.sendEmail(email,"ע��������֤",sdf.format(new Date()));
                newIdentifyBoxPop(2);
            }
            @Override
            public void mousePressed(MouseEvent e) { }

            @Override
            public void mouseReleased(MouseEvent e) { }

            @Override
            public void mouseEntered(MouseEvent e) {
                labels[4].setForeground(Color.RED);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                labels[4].setForeground(Color.black);
            }
        });

        buttons[0].setLabel("���ص�¼");
        buttons[0].setBounds(150,510,120,50);
        buttons[0].setFont(new Font("����", Font.PLAIN, 20));
        RegisterWindow rw = this;
        buttons[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i = 0 ; i < labels.length ; i++)
                    remove(labels[i]);
                for(int i = 0 ; i < textFields.length ; i++)
                    remove(textFields[i]);
                for(int i = 0 ; i < buttons.length ; i++) {
                    remove(buttons[0]);
                }
                rw.dispose();
                loginWindow.init();
            }
        });

        buttons[1].setLabel("ע��");
        buttons[1].setEnabled(false);
        buttons[1].setBounds(280,510,360,50);
        buttons[1].setFont(new Font("����", Font.PLAIN, 30));
        buttons[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = textFields[0].getText();
                String password = textFields[1].getText();
                String phone = textFields[2].getText();
                String email = textFields[3].getText();
                if(name == null || password == null || phone == null || email == null) {
                    Pop p = new Pop(getThis(),"����Ϊnull");
                    return;
                }
                long promptTemp = UserService.register(name,password,phone,email);
                String promptTemp_String = String.valueOf(promptTemp);
                if(promptTemp > 0) {
                    Pop p = new Pop(getThis(),promptTemp_String);
                }else {
                    byte i = (byte)promptTemp;
                    switch (i) {
                        case ErrorNum.PHONE_FORMAT_ERROR:
                            newPromptBoxPop("�ֻ��Ÿ�ʽ����");
                            break;
                        case ErrorNum.EMAIL_FORMAT_ERROR:
                            newPromptBoxPop("�����ʽ����");
                            break;
                        default:
                            newPromptBoxPop(ErrorNum.getErrorInformation(i));
                            break;
                    }
                }


            }
        });


        for(Label label : labels) {
            add(label);
        }
        for(TextField textField : textFields) {
            add(textField);
        }
        for (Button button : buttons) {
            add(button);
        }

        setVisible(true);


    }

    public void paint(Graphics g) {
        super.paint(g);
        BufferedImage image;
        try {
            image = ImageIO.read(new File(System.getProperty("user.dir") + "\\resource\\picture\\register.jpg"));
            g.drawImage(image, 0, 30, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setEmailOk() {
        labels[3].setText("������: ");
        this.emailOk = true;
        buttons[1].setEnabled(true);
        this.setVisible(true);

    }



    public RegisterWindow getThis() {
        return this;
    }

    public void newPromptBoxPop(String content) {
        if(promptBox != null) {
            promptBox.reloadPop(content);
        }else {
            promptBox = new Pop(this,content);
        }
    }

    public void newIdentifyBoxPop(int type) {
        if(identifyBox != null) {
            identifyBox.validation(type);
        }else {
            identifyBox = new Pop(this,type);
        }
    }
    
    public Pop getPromptBox() {
        return this.promptBox;
    }
    
    public String getIdentifyId() {
        return this.identifyId;
    }



    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        dispose();
        loginWindow.setVisible(true);
        loginWindow.init();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
