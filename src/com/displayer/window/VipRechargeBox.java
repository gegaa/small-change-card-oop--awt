package com.displayer.window;

import com.project.Dao.BillDao;
import com.project.Dao.UserDao;
import com.project.util.ErrorNum;
import com.project.vo.Bill;
import com.project.vo.User;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.regex.Pattern;

public class VipRechargeBox extends Frame implements WindowListener {

    private MainWindow parentFrame;
    private Label label_monthNum;
    private TextField textField_monthNum;
    private Button ok;

    public VipRechargeBox(Frame parentFrame, String labelContent, String title, byte button_type) {

    }

    public VipRechargeBox(MainWindow parentFrame) {
        this.parentFrame = parentFrame;
        this.setTitle("VIP充值");
        label_monthNum = new Label("月数(每月20元): ");
        textField_monthNum = new TextField();
        ok = new Button("确认");

        this.setSize(540,200);
        this.setResizable(true);
        this.setLocationRelativeTo(null);
        this.setLayout(null);

        label_monthNum.setBounds(50,80,150,30);
        label_monthNum.setFont(new Font("仿宋", Font.PLAIN, 20));

        textField_monthNum.setBounds(250,80,150,30);
        textField_monthNum.setFont(new Font("仿宋", Font.PLAIN, 20));

        ok.setBounds(165,130,210,40);
        ok.setFont(new Font("仿宋", Font.PLAIN, 20));

        this.add(label_monthNum);
        this.add(textField_monthNum);
        this.add(ok);

        this.setVisible(true);
        this.addWindowListener(this);
    }

    public void getVipRechargeNum() {
        int resultCode = 0;
        this.ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String num = textField_monthNum.getText();
                int result = 0;
                if(Pattern.compile("^[1-9]*[1-9][0-9]*$").matcher(num).find() && Integer.valueOf(num) > 0)
                    result =  Integer.valueOf(num);
                else {
                    System.out.println(ErrorNum.getErrorInformation(ErrorNum.MONTH_NUM_FORMAT_ERROR));
                    return;
                }
                if(result < 0) {
                    System.out.println("月数格式错误");//newPromptBox(ErrorNum.getErrorInformation((byte)result));
                    return;
                }

                //判断用户余额是否能够满足VIP充值花费
                if(parentFrame.getUserVipEnd().getTime() - System.currentTimeMillis() <= 0) {  //非vip
                    System.out.println("非VIP");
                    if(parentFrame.getUserBalance().doubleValue() -  result*20 < 0) {  //如果用户余额小于支出金额，支出记录加入账单失败
                        System.out.println("您的余额不足！可通过升级为VIP进行透支消费");
                        return;
                    }
                }
                Bill bill  = new Bill("支出",new BigDecimal(result*20),"VIP充值"+result+"个月");

                Timestamp current_timestamp = new Timestamp(System.currentTimeMillis());
                Timestamp final_vipEnd = null;
                //判断用户的endVip日期时间是否大于当前日期时间
                if(parentFrame.getUserVipEnd().getTime() > current_timestamp.getTime()) {  //大于，表示用户已是vip，此时充值vip应该在原先vip日期时间上充值vip时间
                    LocalDateTime added = LocalDateTime.ofInstant(Instant.ofEpochMilli(parentFrame.getUserVipEnd().getTime()), ZoneId.systemDefault()).minus(-30*result, ChronoUnit.DAYS);
                    final_vipEnd = new Timestamp(added.toInstant(ZoneOffset.of("+8")).toEpochMilli());
                } else {  //小于或等于，表示用户不是vip，此时充值vip应该在当前日期时间基础上加上充值vip日期时间
                    LocalDateTime added= LocalDateTime.now().minus(-30*result, ChronoUnit.DAYS);
                    final_vipEnd = new Timestamp(added.toInstant(ZoneOffset.of("+8")).toEpochMilli());
                }

                if(final_vipEnd == null)  return;
//                User userTemp = new User(parentFrame.getUserId(),
//                        parentFrame.getUserBalance().add(bill.getMoneyNum()),
//                        final_vipEnd);
                User userTemp = parentFrame.getUser();
                userTemp.setBalance(userTemp.getBalance().add(bill.getMoneyNum()));
                userTemp.setVipEnd(final_vipEnd);
                int resultCode = UserDao.updateUserVip(userTemp);
                if(resultCode != 1) {
                    System.out.println(ErrorNum.getErrorInformation((byte)resultCode));
                    return;
                }

                int resultCode1 = BillDao.inputBill(bill,parentFrame.getUserId());
                if(resultCode1 == 1){
                    System.out.println("账单加入成功");
                }
                else
                    System.out.println(ErrorNum.getErrorInformation((byte)resultCode));

                parentFrame.setUser(userTemp);
                parentFrame.selectList();
                parentFrame.isVip();
                setVisible(false);
                parentFrame.setVisible(true);
            }
        });

    }

//
//    this.setTitle("VIP充值");
//                label.setText("月数");
//                label.setBounds(30,80,80,30);
//                textField.setBounds(110,80,150,30);
//                label.setFont(new Font("仿宋",Font.PLAIN, 20));
//                textField.setFont(new Font("仿宋", Font.PLAIN, 20));
//                this.add(label);
//                this.add(textField);
//                this.addWindowListener(normal);

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        this.setVisible(false);
        this.parentFrame.setVisible(true);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

}
