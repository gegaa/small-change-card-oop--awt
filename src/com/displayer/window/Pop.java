package com.displayer.window;

import com.project.util.SendEmail;

import java.awt.*;
import java.awt.event.*;

public class Pop extends Dialog {
    public static final int PROMPT_BOX = 0;
    public static final int PHONE_IDENTIFY_BOX = 1;
    public static final int EMAIL_IDENTIFY_BOS = 2;

    private int status;
    private Label label = new Label();
    private TextField textField = new TextField();
    private Button[] buttons = new Button[3];
    private WindowListener windowListener = new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
            off();
        }
    };
    private WindowListener normal = new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
            dispose();
        }
    };
    private Label codeErrod = new Label("验证码错误");

    private String prompt;

    public Pop(Frame owner,int status) {
        super(owner,true);
        this.status = status;

        this.setSize(320,200);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(null);

        for(int i = 0 ; i < buttons.length ; i++)
            buttons[i] = new Button();

        validation(status);
    }

    /**
     * 提示对话框所对应的构造方法
     * @param ower
     * @param prompt
     */
    public Pop(Frame ower,String prompt) {
        super(ower,true);
        if (prompt == null) throw new NullPointerException();
        this.prompt = prompt;

        this.setTitle("提示");
        this.setSize(320,200);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setLayout(null);

        label.setText(this.prompt);
        label.setBounds(80,80,150,30);
        label.setFont(new Font("仿宋", Font.PLAIN, 20));

        this.add(label);
        this.addWindowListener(normal);

        this.setVisible(true);
    }

    public void reloadPop(String content){
        this.label.setText(content);
        this.setVisible(true);
    }


    public void validation(int type) {
        switch (type) {
            case 1:
                this.setTitle("手机号验证");
                buttons[0].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
//                        if(SendEmail.identifyEmail(((RegisterWindow)getParent()).getIdentifyId(),textField.getText())) {
//                            dispose();
//                            ((RegisterWindow)getParent()).setVisible(true);
//                        }else {
//                            label.setBounds(90,100,60,15);
//                            label.setFont(new Font("仿宋", Font.PLAIN, 15));
//                            add(label);
//                        }
                    }
                });
                this.setVisible(true);
                break;
            case 2:
                this.setTitle("邮箱验证");
                buttons[0].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(SendEmail.identifyEmail(((RegisterWindow)getParent()).getIdentifyId(),textField.getText())) {
                            dispose();
                            ((RegisterWindow)getParent()).setEmailOk();
                            ((RegisterWindow)getParent()).setVisible(true);
                        }else {
                            label.setBounds(90,100,60,15);
                            label.setFont(new Font("仿宋", Font.PLAIN, 15));
                            add(label);
                        }
                    }
                });
                break;
        }

        label.setText("验证码: ");
        label.setBounds(30,80,80,30);
        label.setFont(new Font("仿宋", Font.PLAIN, 20));

        textField.setBounds(110,80,150,30);
        textField.setFont(new Font("仿宋", Font.PLAIN, 20));

        buttons[0].setLabel("验证");
        buttons[0].setBounds(40,130,210,40);
        buttons[0].setFont(new Font("仿宋", Font.PLAIN, 20));

        this.add(label);
        this.add(textField);
        this.add(buttons[0]);

        this.addWindowListener(windowListener);

        this.setVisible(true);
    }

    public String getInputCode() {
        return textField.getText();
    }


    public void off() {
        this.removeAll();
        this.removeWindowListener(windowListener);

        label.setText("是否退出验证？");
        label.setBounds(80,80,150,30);

        buttons[1].setLabel("确定");
        buttons[1].setBounds(40,130,100,30);
        buttons[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
            }
        });

        buttons[2].setLabel("取消");
        buttons[2].setBounds(150,130,100,30);
        buttons[2].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeAll();
                addWindowListener(windowListener);
                validation(status);
            }
        });

        this.add(label);
        this.add(buttons[1]);
        this.add(buttons[2]);

        this.setVisible(true);
    }

}
