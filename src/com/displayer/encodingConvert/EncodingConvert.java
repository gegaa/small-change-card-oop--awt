package com.displayer.encodingConvert;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class EncodingConvert {
    public static String convert(String orignal, String encoding) {
        try {
            byte[] orignalBytes = orignal.getBytes(Charset.defaultCharset());
            return new String(orignalBytes,encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
